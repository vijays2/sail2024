global    _start
section   .text
_start:   
    mov rax, 1
    mov rbx, 1
    add       rax, rbx
    mov       rax, 60 ; system call for exit
    xor       rdi, rdi ; exit code 0 (XOR is an operation on a number, XORing the same number gives you zero)
    syscall ; invoke operating system to exit (this is added to your code by the compiler and needs to happen)
section   .data