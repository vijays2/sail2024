global    _start
global empty_space


section   .text
_start:   
    mov rax, 1 ; arbitrary value so we do an operation during the loop
    mov rbx, empty_space ; these are arbitrary so we do an operation in the loop
    mov rcx, 0 ; this is i
    mov rdx, 1000 ; this is our loop maximum
loop_top:
    cmp rcx, rdx ; this is the same as i = 1000 (or whatever your maximum value in the loop is) 
    je exit ; in assembly the i = 1000 gets broken up
    mov rax, [rbx] 
    add rbx, 1
    add rcx, 1
    jmp loop_top 
exit: 
    add       rax, rbx
    mov       rax, 60 ; system call for exit
    xor       rdi, rdi ; exit code 0 (XOR is an operation on a number, XORing the same number gives you zero)
    syscall ; invoke operating system to exit (this is added to your code by the compiler and needs to happen)
section   .data
empty_space: db 0 ; a little janky this may fail, if it does let us know