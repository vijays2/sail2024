#include <stdio.h>
#include <stdlib.h>

int factorial(int n) {
    if (n < 0) return -1;
    if (n == 0) return 1;
    int sum = 1;
    for (int i = 1; i <= n; i++) {
        sum *= i;
    }
    return sum;

}

int main(int argc, char** argv) {
    if (argc != 2) {
        printf("usage: ./factorial number\n");
    }
    else {
        int result = factorial(atoi(argv[1]));
        if (result == -1) {
            printf("you can't take the factorial of a negative number!\n");
        } else {
            printf("factorial result: %d\n", result);
        }
    }
    return 0;
}